The map of last glacial maximum and the Bering Land Bridge (aka Beringia) at the time of the Last Glacial Maximum (18,000 ybp) made by Petr Baranovskiy for a chapter by Dr. Ken Coates in a book about Native Americans (First Natioons). 


Data sources:

Esri UK Education. 2017. “Last Glacial Maximum Shapefile.” https://www.arcgis.com/home/item.html?id=90bed26927d84fe69479f995e26ad8cc.

Mississippi Open Data. 2019. “Bering Land Bridge Shapefile.” https://opendata.gis.ms.gov/datasets/db04408e0f7049c2b8a3d9a5707d7e21/geoservice?geometry=-322.734%2C22.594%2C37.266%2C81.093. 

naturalearthdata.com. n.d. “Land Polygons Including Major Islands.” https://www.naturalearthdata.com/http//www.naturalearthdata.com/download/110m/physical/ne_110m_land.zip.


R packages: 

Hadley Wickham. 2017. "tidyverse: Easily Install and Load the 'Tidyverse'." R package version 1.2.1. https://CRAN.R-project.org/package=tidyverse.

Pebesma, Edzer. 2018. "Simple Features for R: Standardized Support for Spatial Vector Data." The R Journal 10 (1), 439-446. https://doi.org/10.32614/RJ-2018-009.

Pebesma, Edzer. 2019. "lwgeom: Bindings to Selected 'liblwgeom' Functions for Simple Features." R package version 0.1-7. https://CRAN.R-project.org/package=lwgeom.

Tennekes, M. 2018. “tmap: Thematic Maps in R.” Journal of Statistical Software, 84(6), 1-39. doi: 10.18637/jss.v084.i06. https://doi.org/10.18637/jss.v084.i06.

